# fastapi_react_todo_01

## Getting started

### Start Backend API
```bash
cd  server

# First time install python and pip packages via pipenv
pipenv shell
pipenv install

# Run
python main.py
```


### Start Frontend
We assume that you use NVM (Network Version Manager) to install nodeJS.
Good source: http://www.boekhoff.info/how-to-install-nodejs-and-npm-using-nvm-and-zsh-nvm/

```bash
cd client

## First time install nodejs 
# lists all locallay installed NodeJS versions
nvm ls

# lists all available versions of NodeJS
nvm ls-remote 

# to keep your installation of NVM up to date
nvm upgrade

# to install the latest version of NodeJS 
#nvm install stable

# Install the latest LTS version of NodeJS
nvm install --lts

# Install the latest 8.x.x version of NodeJS
#nvm install 8

# Use the latest stable version of NodeJS
nvm use --lts

# Install dependencies form package.json
npm install

nmp start

```