from sqlalchemy import Column, Integer, String
from database import Base

# Define To Do class inheriting from Base
class ToDo(Base):
    __tablename__ = 'todo'
    id = Column(Integer, primary_key=True)
    title = Column(String(256))
    description = Column(String(256))
